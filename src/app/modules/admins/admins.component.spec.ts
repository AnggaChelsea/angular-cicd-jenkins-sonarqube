import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsComponent } from './admins.component';

describe('Admin: component', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdminsComponent]
    });
  })
  it('should create admin component', () => {
    let fixture = TestBed.createComponent(AdminsComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  })
});
