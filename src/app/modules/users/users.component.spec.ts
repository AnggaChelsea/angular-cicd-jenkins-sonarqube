import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { UsersService } from './users.service';

describe('Component: User', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UsersComponent]
    });
  })
  it('should create the user', () => {
    let fixture = TestBed.createComponent(UsersComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  })
  it('should use the user name from service', () => {
    let fixture = TestBed.createComponent(UsersComponent);
    let app = fixture.debugElement.componentInstance;
    let userService = fixture.debugElement.injector.get(UsersService)
    fixture.detectChanges(); //sincronous
    expect(userService.user.name).toEqual(app.user.name)
  })
  it('shpuld display user name user login', () => {
    let fixture = TestBed.createComponent(UsersComponent);
    let app = fixture.debugElement.componentInstance;
    app.isLogin = true;
    fixture.detectChanges()
    let compile = fixture.debugElement.nativeElement;
    expect(compile.querySelector('p').textContent).toContain(app.user.name)
  })
  // it('should\t display user is not logged in', () => {
  //   let fixture = TestBed.createComponent(UsersComponent);
  //   let app = fixture.debugElement.componentInstance;
  //   fixture.detectChanges()
  //   let compile = fixture.debugElement.nativeElement;
  //   expect(compile.querySelector('p').textContent).not.toContain(app.user.name)
  // })
  // it('should fetch data succefully if not called asynconously', async() => {
  //   let fixture = TestBed.createComponent(UsersComponent);
  //   let app = fixture.debugElement.componentInstance;
  //   let dataService = fixture.debugElement.injector.get(DataService)
  //   let spy = spyOn(dataService, 'getDetails')
  //   .and.returnValue(Promise.resolve('Data'));
  //   fixture.detectChanges();
  //   fixture.whenStable().then(()=> {
  //     expect(app.data).toBe('Data')
  //   })
  // })
});
